// console.log("hewwo!~");

// From instruction 1 - 12 : Fetch

/* 3-4
	- Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

	- Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.*/

fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
	// .then(response => response.json())
	.then(response => {
		console.log(response);
		return response.json()
		})
	// .then(result => console.log(result));
	.then(data => {
		let title = data.map(element => element.title);
		// console.log(typeof title);
		console.log(title);
		})


/* 5-6
	- Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

	- Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
*/
fetch("https://jsonplaceholder.typicode.com/todos/6", {method: "GET"})
	.then(response => response.json())
	.then(result => {
		let title = result.title;
		let status = result.completed;
		// console.log(typeof title);
		// console.log(title);
		// console.log(typeof status);
		// console.log(status);
		console.log(result);
		console.log(`The item "${title}"" on the list has a status of "${status}"`)
		});
// console.log(`The item ${result.title} on the list has a status of ${result.sompleted}`);


/* 7
	- Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "POST Item",
		completed: false,
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));


/* 8-9
	- Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

	- Update a to do list item by changing the data structure to contain the following properties:
			- Title
			- Description
			- Status
			- Date Completed
			- User ID
*/
fetch("https://jsonplaceholder.typicode.com/todos/5", {
		method: "PUT",
		headers: {
			'Content-Type' : "application/json"
		},
		body: JSON.stringify({
			title: "PUT Item",
			description: "Item has been PUT.",
			status: "pending",
			dateCompleted: "n/a",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(result => console.log(result));



/* 10-11
	- Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

	- Update a to do list item by changing the status to complete and add a date when the status was changed.
*/
fetch("https://jsonplaceholder.typicode.com/todos/6", {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "PATCH Item!",
			description: "Item has been PATCHed.",
			status: "completed",
			dateCompleted: "01/31/23",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(result => console.log(result));


/* 12
	- Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos/2", {method: "DELETE"})
	.then(response => response.json())
	.then(result => console.log(result));



// From instruction 13 - 19: Postman
